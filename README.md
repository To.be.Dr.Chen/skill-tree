# Skill Tree

This is the skill tree that I master for my academic goal

# Academic Background

I graduated my bachelor from BeiHang University(BUAA, previously known as Beijing University of Aeronautics and Astronautics), majoring in Mechanism and Automation.

My master will be finished in Karlsruhe Institut of technology, majoring in medicine technology and microsystem.

# Academic Interests
In the long course of human existence and development, nature has always been the best teacher. As a medium connecting man and nature, biological signals manifest the trace for human beings to explore, imitate, and utilize the demiurgic power. On the other hand, high efficiency is always the first priority of technological development. MEMS technology provides a more effective and environmentally friendly way to promote the relationship between man and nature. Combining the two underlying factors, I am prompted to find a Ph.D. position in the Biomems field to obtain an admission ticket for research and contribute to exploring of the unknown.

To realize the academic goal, I have designed a skill tree for myself. And Right now, I have completed the needed skills in the "Tree" for long-time study. And the next step is only to obtain a PhD position and find an entry point for my research career!


# Skills Statement

The skills structure is hierarchical.
The first stage of skills starting from mechanical, electronic, and information knowledge forming a solid base for advanced skills.

The seconde stage is the advanced skills during my master study. I have finished two main projects during my master periods:
1. My experience as a student assistant in IBT(Institut für biomedizinische Technologie) focuses on the cardio signal process, is based on the combination of electronic and information technologies, supplemented by basic biological knowledge. I got the skills from building up an interface for biosignal sampling to signal processing with analog and digital methods from this project.

2. My master thesis is complicated in IMT(Institut für Mikrostruktur Technologie), obtaining advanced skills based on mechanics and electronics, supplemented by basic biological and chemical knowledge. With this, I have mastered many microstructure techniques and devices, including lithography, lamination, 3D-Printing, laser cutting, and sufficient experience in the cleanroom.

# Academical goal
PhD would be the third phase of my academic life. Biosensing and Biomems system are my ideal research area, which can combine my advanced skills.
I wish I could find a position in this area and finish something to contribute to the development of the field!


